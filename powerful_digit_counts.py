import time
start_time = time.time()

def find_count_powerful_digit():
    count = 0
    for i in range(1,10):
        for j in range(1,22): 
            count += (len(str(i**j)) == j)
    return count

print(find_count_powerful_digit())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )